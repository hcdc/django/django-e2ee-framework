# Changelog

## v0.0.1.dev1: Fix session key management
Overwrite existing session keys.

Changed
-------
- The session keys are now recreated when there are already existing ones,
  see [!6](https://codebase.helmholtz.cloud/hcdc/django/django-e2ee-framework/-/merge_requests/6)


## v0.0.1.dev0: Initial release
First official release on pypi.org
