"""pytest configuration script for django-e2ee-framework."""

# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of the project django-e2ee-framework
# and is released under the EUPL-1.2 license. See LICENSE in the root of the
# repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.
from __future__ import annotations

import json
import os
from itertools import count
from typing import TYPE_CHECKING, Callable, Dict, Optional

import pytest
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

if TYPE_CHECKING:
    from django.contrib.auth.models import User

if (
    os.getenv("RUNNING_TESTS")
    and os.getenv("POSTGRES_DB") == "ACADEMIC_COMMUNITY_TEST_DB"
):

    @pytest.fixture(scope="session")
    def django_db_setup():
        pass


def pytest_addoption(parser):
    """Parse pytest --option variables from shell"""
    parser.addoption("--browser", help="Which test browser?", default="chrome")
    parser.addoption(
        "--ci-tests",
        help="running tests within docker-compose?",
        action="store_true",
    )


@pytest.fixture(scope="session")
def test_browser(request):
    """:returns Browser.NAME from --browser option"""
    return request.config.getoption("--browser")


@pytest.fixture(scope="session")
def running_in_ci(request):
    """:returns true or false from --local option"""
    return os.getenv("GITLAB_CI") or request.config.getoption("--ci-tests")


@pytest.fixture(scope="function")
def remote_browser_factory(  # type: ignore
    test_browser: str, running_in_ci: bool
) -> Callable[[], webdriver.Remote]:
    """Select configuration depends on browser and host"""

    drivers = []

    def remote_browser() -> webdriver.Remote:
        driver: webdriver.Remote
        if running_in_ci:
            options = webdriver.ChromeOptions()
            options.add_argument("--no-sandbox")
            options.add_argument("--headless")
            driver = webdriver.Chrome(options=options)
        else:
            if test_browser == "firefox":
                driver = webdriver.Firefox()
            elif test_browser == "chrome":
                driver = webdriver.Chrome()
        drivers.append(driver)
        return driver

    yield remote_browser

    for driver in drivers:
        driver.quit()


@pytest.fixture
def dummy_password() -> str:
    return "some_password"


@pytest.fixture
def test_user_factory(
    db, django_user_model, dummy_password: str
) -> Callable[[], User]:

    from django.contrib.auth.models import Permission

    counter = count()

    def factory():
        i = next(counter)
        first_name = f"Max{i}"
        last_name = f"Mustermann{i}"
        user = django_user_model.objects.create(
            username=f"{first_name}_{last_name}",
            first_name=first_name,
            last_name=last_name,
            password=dummy_password,
            is_staff=True,
        )
        perm1 = Permission.objects.get_by_natural_key(
            "add_masterkey", "e2ee", "masterkey"
        )
        perm2 = Permission.objects.get_by_natural_key(
            "view_masterkey", "e2ee", "masterkey"
        )
        user.user_permissions.add(perm1, perm2)
        user.set_password(dummy_password)
        user.save()
        return user

    return factory


@pytest.fixture
def make_request() -> Callable[
    [webdriver.Remote, str, str, Optional[Dict]], Dict
]:
    """Fixture to make request via the RestAPI."""

    def make_request(
        driver: webdriver.Remote,
        uri: str,
        method: str,
        body: Optional[Dict] = None,
    ) -> Dict:
        """Make a request to the backend."""
        # manually refresh the page because for whatever reasons the service worker
        # routing may not work otherwise
        driver.refresh()

        driver.execute_script("window.location = window.location")

        WebDriverWait(driver, 10).until(
            EC.text_to_be_present_in_element(
                (By.ID, "serviceworker-report"), "Serviceworker registered"
            )
        )

        token = driver.get_cookie("csrftoken")["value"]  # type: ignore
        if body is not None:
            str_body = f"body: '{json.dumps(body)}',"
        else:
            str_body = ""

        js = """
            function makeRequest() {
                return fetch(
                    '%(uri)s',
                    {
                        method: "%(method)s", %(body)s
                        headers: {
                            'X-CSRFTOKEN': '%(token)s',
                            'Content-Type': 'application/json'
                        }
                    }
                ).then(async d => [d, await d.json()]);
            }
            return makeRequest();
        """ % dict(
            uri=uri, token=token, body=str_body, method=method
        )

        response_body = None
        # for whatever reason, the response may be None and the request has not
        # been made. therefore we make requests until we get a result
        while response_body is None:
            response_body = driver.execute_script(js)
        return response_body[0], response_body[1]

    return make_request


@pytest.fixture
def test_user(test_user_factory: Callable[[], User]) -> User:
    return test_user_factory()


@pytest.fixture
def live_reverse(live_server) -> Callable[[str], str]:
    """Get a callable that reverses a url and attaches the live server."""

    def live_reverse(*args, **kwargs):
        return str(live_server) + reverse(*args, **kwargs)

    return live_reverse


@pytest.fixture
def authenticated_browser_factory(
    remote_browser_factory, live_reverse, dummy_password: str
) -> Callable[[User], webdriver.Remote]:
    def browser_factory(user: User) -> webdriver.Remote:

        remote_browser = remote_browser_factory()
        remote_browser.get(live_reverse("admin:login"))
        username_input = remote_browser.find_element(By.NAME, "username")
        username_input.send_keys(user.username)
        password_input = remote_browser.find_element(By.NAME, "password")
        password_input.send_keys(dummy_password)
        remote_browser.find_element(
            By.XPATH, '//input[@value="Log in"]'
        ).click()
        remote_browser.get(live_reverse("test_app:home"))
        WebDriverWait(remote_browser, 10).until(
            EC.text_to_be_present_in_element(
                (By.ID, "serviceworker-report"), "Serviceworker registered"
            )
        )
        return remote_browser

    return browser_factory


@pytest.fixture
def authenticated_browser(
    authenticated_browser_factory, test_user
) -> webdriver.Remote:
    """Get the browser with the :func:`test_user` logged in."""
    return authenticated_browser_factory(test_user)


@pytest.fixture
def e2e_browser_factory(
    authenticated_browser_factory: webdriver.Remote,
    make_request: Callable[[webdriver.Remote, str, str, Optional[Dict]], Dict],
    dummy_password: str,
) -> Callable[[User], webdriver.Remote]:
    """Get a browser with an e2e-password set."""

    def browser_factory(user: User) -> webdriver.Remote:
        authenticated_browser = authenticated_browser_factory(user)
        response, body = make_request(
            authenticated_browser,
            reverse("e2ee:masterkey-list"),
            "POST",
            {"password": dummy_password, "identifier": "test"},
        )

        assert response
        assert response["status"] == 201
        return authenticated_browser

    return browser_factory


@pytest.fixture
def e2e_browser(
    authenticated_browser: webdriver.Remote,
    make_request: Callable[[webdriver.Remote, str, str, Optional[Dict]], Dict],
    dummy_password: str,
) -> webdriver.Remote:
    """Get a browser with an e2e-password set."""
    response, body = make_request(
        authenticated_browser,
        reverse("e2ee:masterkey-list"),
        "POST",
        {"password": dummy_password, "identifier": "test"},
    )

    assert response
    assert response["status"] == 201
    return authenticated_browser
