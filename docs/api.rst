.. _api:

API Reference
=============

.. toctree::
    :maxdepth: 1

    api/django_e2ee.app_settings
    api/django_e2ee.urls
    api/django_e2ee.models
    api/django_e2ee.views


.. toctree::
    :hidden:

    api/django_e2ee
