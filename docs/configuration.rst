.. _configuration:

Configuration options
=====================

Configuration settings
----------------------

The following settings have an effect on the app.

.. automodulesumm:: django_e2ee.app_settings
    :autosummary-no-titles:
