.. django-e2ee-framework documentation master file, created by
   sphinx-quickstart on Mon Feb 21 15:15:53 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-e2ee-framework's documentation!
=================================================

.. rubric:: An end-to-end encryption framework for Django

.. warning::

    This package is work in progress, especially it's documentation.
    Stay tuned for updates and discuss with us at
    https://gitlab.hzdr.de/hcdc/django/django-e2ee-framework


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   configuration
   api
   contributing



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
