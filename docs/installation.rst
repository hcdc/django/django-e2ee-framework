.. _installation:

Installation
============

To install the `django-e2ee-framework` package for your Django project, you need
to follow two steps:

1. :ref:`Install the package <install-package>`
2. :ref:`Add the app to your Django project <install-django-app>`

.. _install-package:

Installation from PyPi
----------------------
The recommended way to install this package is via pip and PyPi via::

    pip install django-e2ee-framework

Or install it directly from `the source code repository on Gitlab`_ via::

    pip install git+https://gitlab.hzdr.de/hcdc/django/django-e2ee-framework.git

The latter should however only be done if you want to access the development
versions.

.. _the source code repository on Gitlab: https://gitlab.hzdr.de/hcdc/django/django-e2ee-framework


.. _install-django-app:

Install the Django App for your project
---------------------------------------
To use the `django-e2ee-framework` package in your Django project,
you need to add the app to your `INSTALLED_APPS`, configure your `urls.py`, run the
migration, add a login button in your templates. Here are the step-by-step
instructions:

1. Add the `django_e2ee` app to your `INSTALLED_APPS`
2. in your projects urlconf (see :setting:`ROOT_URLCONF`), add include
   :mod:`django_e2ee.urls` via::

       from django.urls import include, path

       urlpatterns += [
           path("django-e2ee-framework/", include("django_e2ee.urls")),
        ]
3. Run ``python manage.py migrate`` to add models to your database
4. Configure the app to your needs (see :ref:`configuration`).

That's it! For further adaption to you Django project, please head over to the
:ref:`configuration`. You can also have a look into the ``testproject``
in the `source code repository`_ for a possible implementation.

.. _source code repository: https://gitlab.hzdr.de/hcdc/django/django-e2ee-framework


.. _install-develop:

Installation for development
----------------------------
Please head over to our :ref:`contributing guide <contributing>` for
installation instruction for development.
