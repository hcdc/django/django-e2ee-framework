"""Management command to create dummy users Alice and Bob."""
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    """Django command to migrate the database."""

    help = "Create dummy users alice and bob"

    def handle(self, *args, **options):
        from django.contrib.auth import get_user_model

        User = get_user_model()

        alice = User.objects.create(username="alice")
        alice.set_password("alice")
        alice.is_staff = True
        alice.save()

        bob = User.objects.create(username="bob")
        bob.set_password("bob")
        bob.is_staff = True
        bob.save()
