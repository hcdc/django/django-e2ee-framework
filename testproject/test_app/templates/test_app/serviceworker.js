{% load static %}

importScripts(
  'https://storage.googleapis.com/workbox-cdn/releases/6.4.1/workbox-sw.js'
);

self.addEventListener("install", event => {
  console.log("installing service worker")
});

self.addEventListener('activate', event => {
  console.log("activating service worker")
});

{% include "e2ee/serviceworker_routes.js" %}
