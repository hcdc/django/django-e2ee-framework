from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

from django_e2ee.views import E2ELoginViewMixin


class HomeView(LoginRequiredMixin, E2ELoginViewMixin, generic.TemplateView):
    """A base view for the test app."""

    template_name = "test_app/home.html"


class ServiceWorkerView(generic.TemplateView):
    """A view to render the webmanifest"""

    template_name = "test_app/serviceworker.js"

    content_type = "application/javascript"
