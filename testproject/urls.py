"""django_e2ee URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

# Disclaimer
# ----------
#
# Copyright (C) 2022 Helmholtz-Zentrum Hereon
#
# This file is part of django-e2ee-framework and is released under the
# EUPL-1.2 license.
# See LICENSE in the root of the repository for full licensing details.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the EUROPEAN UNION PUBLIC LICENCE v. 1.2 or later
# as published by the European Commission.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# EUPL-1.2 license for more details.
#
# You should have received a copy of the EUPL-1.2 license along with this
# program. If not, see https://www.eupl.eu/.


from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.generic import RedirectView


class PatchRedirectView(RedirectView):
    """Patched redirect view to avoid issues with FORCE_SCRIPT_NAME"""

    def get_redirect_url(self, *args, **kwargs):
        ret = self.request.path[1:]
        script_name = settings.FORCE_SCRIPT_NAME[1:]
        while ret.startswith(script_name):
            ret = ret[len(script_name) :]
        return "/" + ret


urlpatterns = [
    path("", include("django.contrib.auth.urls")),
    path("", include("testproject.test_app.urls")),
    path(
        "e2ee/",
        include("django_e2ee.urls"),
    ),
    path("admin/doc/", include("django.contrib.admindocs.urls")),
    path("admin/", admin.site.urls),
]

if getattr(settings, "FORCE_SCRIPT_NAME", None):
    urlpatterns.append(
        re_path(  # type: ignore
            settings.FORCE_SCRIPT_NAME[1:] + ".*", PatchRedirectView.as_view()
        )
    )


# # This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns += static(  # type: ignore
        "static/", document_root=settings.STATIC_ROOT
    )
